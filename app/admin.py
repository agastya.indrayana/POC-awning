from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm

from app.models import Articles, Attachments, Categories, Products, Tags, User


class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User

class MyUserAdmin(UserAdmin):
    form = MyUserChangeForm

    fieldsets = UserAdmin.fieldsets + (
            (None, {'fields': ('profile_picture',)}),
    )
admin.site.register(User, MyUserAdmin)

@admin.register(Attachments)
class AttachmentsAdmin(admin.ModelAdmin):
    pass

@admin.register(Tags)
class TagsAdmin(admin.ModelAdmin):
    pass

@admin.register(Categories)
class CategoriesAdmin(admin.ModelAdmin):
    pass

@admin.register(Products)
class ProductsAdmin(admin.ModelAdmin):
    pass

@admin.register(Articles)
class ArticlesAdmin(admin.ModelAdmin):
    pass
