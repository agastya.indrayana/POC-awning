import time
from os import name

from django.contrib.auth.models import (AbstractBaseUser, AbstractUser,
                                        PermissionsMixin, UserManager)
from django.db import models
from django.db.models.fields import BLANK_CHOICE_DASH
from django.templatetags.static import static

from app.templatetags.common_tags import (is_document_file_audio,
                                          is_document_file_code,
                                          is_document_file_image,
                                          is_document_file_pdf,
                                          is_document_file_sheet,
                                          is_document_file_text,
                                          is_document_file_video,
                                          is_document_file_zip)


def img_url(self, filename):
    hash_ = int(time.time())
    return "%s/%s/%s" % ("profile_pics", hash_, filename)

class User(AbstractUser):
    profile_picture = models.ImageField(blank=True, null=True)

    @property
    def profile_picture_url(self):
        profile_picture = self.profile_picture
        # Pseudocode:
        if bool(profile_picture):
            return profile_picture.url
        else:
            return static('app/image/user-default.png')

class Attachments(models.Model):
    created_by = models.ForeignKey(
        User, related_name='attachment_created_by',
        on_delete=models.SET_NULL, null=True)
    file_name = models.CharField(max_length=256)
    created_on = models.DateTimeField("Created on", auto_now_add=True)
    attachment = models.FileField(
        max_length=1001, upload_to='attachments/%Y/%m/')

    def file_type(self):
        name_ext_list = self.attachment.url.split(".")
        if (len(name_ext_list) > 1):
            ext = name_ext_list[int(len(name_ext_list) - 1)]
            if is_document_file_audio(ext):
                return ("audio", "fa fa-file-audio")
            if is_document_file_video(ext):
                return ("video", "fa fa-file-video")
            if is_document_file_image(ext):
                return ("image", "fa fa-file-image")
            if is_document_file_pdf(ext):
                return ("pdf", "fa fa-file-pdf")
            if is_document_file_code(ext):
                return ("code", "fa fa-file-code")
            if is_document_file_text(ext):
                return ("text", "fa fa-file-alt")
            if is_document_file_sheet(ext):
                return ("sheet", "fa fa-file-excel")
            if is_document_file_zip(ext):
                return ("zip", "fa fa-file-archive")
            return ("file", "fa fa-file")
        return ("file", "fa fa-file")

    def get_file_type_display(self):
        if self.attachment:
            return self.file_type()[1]
        return None

    def __str__(self):
        if self.attachment:
            return f"{self.file_type()[0]} - {self.file_name}"
        return self.file_name


class Categories(models.Model):
    name = models.CharField(max_length=120)
    description = models.TextField()

    def __str__(self):
        return self.name

class Tags(models.Model):
    name = models.CharField(max_length=120)
    description = models.TextField()

    def __str__(self):
        return self.name


class Products(models.Model):
    product_id = models.CharField(max_length=120)
    name = models.CharField(max_length=120)
    descriptions = models.TextField()

    def __str__(self):
        return self.name


class Articles(models.Model):
    # content
    title = models.CharField(max_length=120)
    subtitle = models.TextField()
    content = models.TextField()
    attachments = models.ManyToManyField(Attachments)
    thumbnail = models.ImageField()
    # properties
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
    category = models.ForeignKey(Categories, on_delete=models.SET_NULL, blank=True, null=True)
    tags = models.ManyToManyField(Tags)
    products = models.ManyToManyField(Products, blank=True)

    def __str__(self):
        return self.title
