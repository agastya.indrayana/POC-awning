from django.db.models import fields
from app.models import Articles, Attachments, User
from rest_framework import serializers

from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )

    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('username', 'password', 'password2', 'email', 'first_name', 'last_name', 'profile_picture')
        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True},
            'profile_picture': {'required': True},
        }

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            profile_picture=validated_data['profile_picture']
        )

        
        user.set_password(validated_data['password'])
        user.save()

        return user

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'profile_picture_url']


class Attachmentserializers(serializers.ModelSerializer):
    class Meta:
        model= Attachments
        fields =[
            'file_name','created_on','attachment'
            ]


class ArticlesBriefSerializer(serializers.HyperlinkedModelSerializer):
    category = serializers.StringRelatedField(many=False)
    tags = serializers.StringRelatedField(many=True)
    products = serializers.StringRelatedField(many=True)

    class Meta:
        model = Articles
        fields = [
            'id','title','subtitle','thumbnail','created_on','modified_on','category','tags','products'
            ]


class ArticlesDetailSerializer(serializers.HyperlinkedModelSerializer):
    category = serializers.StringRelatedField(many=False)
    tags = serializers.StringRelatedField(many=True)
    products = serializers.StringRelatedField(many=True)
    attachments = Attachmentserializers(many=True, read_only=True)
    
    class Meta:
        model = Articles
        fields = ['id','title','subtitle','thumbnail','content','attachments','created_on','modified_on','category','tags','products',]