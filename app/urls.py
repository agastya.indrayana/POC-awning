from django.urls import include, path
from rest_framework import routers

from app import views

router = routers.DefaultRouter()
router.register(r'articles', views.ArticlesViewSet)

urlpatterns = [
    path('register/', views.RegisterView.as_view(), name='auth_register'),
    path('user-detail/', views.UserDetailView.as_view(), name='user_detail'),
]
