from django.http import response
from rest_framework import filters, permissions, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication
from rest_framework.generics import CreateAPIView, RetrieveAPIView
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication
from app.models import Articles, User
from app.serializers import ArticlesBriefSerializer, ArticlesDetailSerializer

from .serializers import RegisterSerializer, UserSerializer

from rest_framework.response import Response

class RegisterView(CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (permissions.AllowAny,)
    serializer_class = RegisterSerializer


class UserDetailView(APIView):
    authentication_classes = [JWTAuthentication, SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        serializer = UserSerializer(self.request.user)
        return Response(serializer.data)

class ArticlesViewSet(viewsets.ModelViewSet):
    queryset = Articles.objects.all().order_by('-created_on')
    serializer_class = ArticlesBriefSerializer
    permission_classes = [permissions.DjangoModelPermissionsOrAnonReadOnly]
    search_fields = [
        'title','subtitle','category__name','tags__name','products__name'
    ]
    filter_backends = (filters.SearchFilter,)

    action_serializers = {
        'retrieve': ArticlesDetailSerializer,
        'list': ArticlesBriefSerializer
    }

    def get_serializer_class(self):

        if hasattr(self, 'action_serializers'):
            return self.action_serializers.get(self.action, self.serializer_class)

        return super(ArticlesViewSet, self).get_serializer_class()

